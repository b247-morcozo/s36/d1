const express = require("express");
const router = express.Router();

// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController");


// [ SECTION ] Get all tasks
	// This route expects to receive a GET request at the URL "/tasks"

router.get("/", (req, res) => {

	// It invokes the getAllTasks function from the taskController.js
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});

// [ SECTION ] Create a new task
	// This route expects to receive a POST request at the URL "/tasks"

router.post("/", (req,res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// [ SECTION ] Delete a task
	// This route expects to receive a DELETE request at the URL "/tasks/:id"

router.delete("/:id", (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// [ SECTION ] Update a task
	// This route expects to receive a PUT request at the URL "/tasks/:id"
	
router.put("/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;

//Activity
router.get("/:id", (req,res) => {

    taskController.getTasks(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {

	taskController.statusTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})
